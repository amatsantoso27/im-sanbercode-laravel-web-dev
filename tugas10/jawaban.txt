1. buat database 

create database myshop;

2. Membuat Table di Dalam Database

use myshop;

create table users(id int AUTO_INCREMENT, name varchar(255), email varchar(255), password varchar(255), PRIMARY KEY (id));

create table categories(id int AUTO_INCREMENT, name varchar(255), PRIMARY KEY (id));

create table items(id int AUTO_INCREMENT, name varchar(255), description varchar(255), price int, stock int, category_id int, PRIMARY KEY (id), FOREIGN KEY (category_id) REFERENCES categories(id))

3. Memasukkan Data pada Table

INSERT INTO users (name, email, password) VALUES ('John Doe', 'john@doe.com', 'john123');		
INSERT INTO users (name, email, password) VALUES ('John Doe', 'john@doe.com', 'jenita123');	

INSERT INTO categories (name) VALUES ('gadget');
INSERT INTO categories (name) VALUES ('cloth');
INSERT INTO categories (name) VALUES ('men');
INSERT INTO categories (name) VALUES ('women');
INSERT INTO categories (name) VALUES ('branded');

INSERT INTO items (name, description, price, stock, category_id) VALUES ('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1);
INSERT INTO items (name, description, price, stock, category_id) VALUES ('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2);
INSERT INTO items (name, description, price, stock, category_id) VALUES ('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);


4. Mengambil Data dari Database

	a. mengambil data users 

	SELECT id, name, email FROM users;

	b. mengambil data items
		> Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).
		SELECT * FROM items WHERE price > 1000000;
		
		>Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).
		SELECT * FROM items WHERE items LIKE '%uniklo%';

	c. Menampilkan data items join dengan kategori

	SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori
	FROM items
	INNER JOIN categories 
	ON items.category_id = categories.id;

5. Mengubah Data dari Database
UPDATE items
SET price = 2500000
WHERE name='sumsang b50';