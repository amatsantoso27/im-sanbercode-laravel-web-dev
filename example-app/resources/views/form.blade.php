@extends('layout.master')

@section('title')
    Form Register
@endsection

@section('content')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcom" method="POST">
        @csrf
    <label for="fName">First Name:</label><br><br>
    <input type="text" name="fName" id="fName"><br><br>
    <label for="lName">Last Name:</label><br><br>
    <input type="text" name="lName" id="lName"><br><br>
    <label for="gender">Gender:</label><br><br>
    <input type="radio" name="male" id="male">Male <br>
    <input type="radio" name="female" id="female">Female <br>
    <input type="radio" name="other" id="other">Other <br><br>
    <label for="nationality">Nationality</label><br><br>
    <select name="nationality">
        <option value="indonesian">Indonesian</option>
        <option value="singaporean">Singaporean</option>
        <option value="malaysian">Malaysian</option>
        <option value="australian">Australian</option>
    </select><br><br>
    <label for="language">Language Spoken:</label><br><br>
    <input type="checkbox" name="indo" id="indo">Bahasa Indonesia <br>
    <input type="checkbox" name="englis" id="englis">English <br>
    <input type="checkbox" name="other" id="otherLang">Other <br><br>
    <label for="bio">Bio:</label> <br><br>
    <textarea name="bio" id="bio" cols="30" rows="10"></textarea> <br>
    <input type="submit" value="Sign Up">
    </form>
@endsection