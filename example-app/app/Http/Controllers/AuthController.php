<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('form');
    }

    public function welcom(Request $request)
    {
        $namaDepan = $request -> input('fName');
        $namaBelakang = $request -> input('lName');
        return view('welcom', ["namaDepan" => $namaDepan, "namaBelakang" => $namaBelakang]);
    }
}
